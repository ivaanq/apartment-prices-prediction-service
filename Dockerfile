FROM python:3.9

RUN pip install --upgrade pip


COPY requirements.txt /requirements.txt
COPY infer.py /infer.py

RUN pip install -r /requirements.txt


CMD ["uvicorn", "infer:app", "--host", "0.0.0.0", "--port", "8001"]