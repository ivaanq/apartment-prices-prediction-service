# apartment-prices-prediction-service

This is service for the model from [apartment-prices-prediction project](https://gitlab.com/ivaanq/apartment-prices-prediction).


### How to run
1) set up mlfow/s3 and so on from [apartment-prices-prediction project](https://gitlab.com/ivaanq/apartment-prices-prediction) via docker-compose

2) run container with api

```
sudo docker build . -t infer
sudo docker run -network host infer
```

3) send requests

```
import requests
file = {'file': open('<filename>.csv','rb'), 'filename': 'test.csv'}
r = requests.post('<url>/invocations', files=file)
r.text
```
