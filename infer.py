from fastapi import FastAPI, UploadFile, File
import mlflow
import os
import pandas as pd


os.environ["AWS_ACCESS_KEY_ID"] = "minioadmin"
os.environ["AWS_SECRET_ACCESS_KEY"] = "minioadmin"
os.environ["MLFLOW_S3_ENDPOINT_URL"] = "http://127.0.0.1:9000"

mlflow.set_tracking_uri("http://127.0.0.1:5000")

app = FastAPI()


class PricePredictor:
    def __init__(self, model_name, model_stage):
        """
        initializes model from mlflow
        Params:
            model_name (str): name of model in registery
            model_stage (str): stage of the model
        """
        self.model = mlflow.pyfunc.load_model(f'models:/{model_name}/{model_stage}')

    def predict(self, data):
        """
        predict prices from given data
        Params:
            data (pd.DataFrame): data to perform predictions
        """
        pred = self.model.predict(data)
        return pred


model = PricePredictor('test_catboost', '1')


@app.post("/invocations")
async def predict_from_csv(file: UploadFile = File(...)):

    with open(file.filename, 'wb') as f:
        f.write(file.file.read())

    data = pd.read_csv(file.filename)
    X = data[data.columns[data.columns != "price"]]
    os.remove(file.filename)

    return list(model.predict(X))
